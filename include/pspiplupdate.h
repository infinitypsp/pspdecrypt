#ifndef __PSPIPLUPDATE_H_
#define __PSPIPLUPDATE_H_

/**
	Clear IPL on NAND

	@return < 0 on error
*/
int sceIplUpdateClearIpl(void);

/**
	Write IPL to NAND
	@param ipl pointer to IPL to write to NAND
	@param size the size of the IPL to write

	@return < 0 on error
*/
int sceIplUpdateSetIpl(const u8 *ipl, unsigned int size);

#endif // __PSPIPLUPDATE_H_
